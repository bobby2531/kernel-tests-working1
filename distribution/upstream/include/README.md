# LTP upstream library
Ported LTP include library for Beaker upstream testing.

## How to run it
Please refer to the top-level README.md for common dependencies. For a complete detail, see PURPOSE file. 

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```
### Execute the test
```bash
$ bash ./runtest.sh
```
